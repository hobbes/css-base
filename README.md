# CSS Design System

Work in progress...

Reusable base for CSS design system. Makes heavy use of css variable to keep the CSS dry and changes and theming easy.

Contains defaults for:
- typography
- colors
- sizes

Includes [normalize.css](https://necolas.github.io/normalize.css/) and opiniated defaults.

## development / preview

- If you use Visual Studio Code as your editor, setup [Dev Container](https://code.visualstudio.com/docs/devcontainers/tutorial) for easy creation of development environment.
- Or install node.js
- Open folder in Dev Container
- Install dependencies: `npm install`
- start development server: `npm start`


## Inspiration:

- https://www.infoq.com/news/2020/06/css-variables-design-systems/
- https://ionicframework.com/docs/theming/color-generator
- https://joshuatz.com/posts/2021/common-css-color-variables-properties-theming/
- https://www.figma.com/best-practices/typography-systems-in-figma/